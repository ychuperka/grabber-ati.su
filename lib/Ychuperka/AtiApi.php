<?php
/**
 * ati.su API
 *
 * @author Yegor Chuperka
 */

namespace Ychuperka;


use Ychuperka\AtiApi\Exception;
use Ychuperka\Grabber\DataProvider\PhantomJsDataProvider;

class AtiApi {

    /**
     * Phantom js data provider
     *
     * @var Grabber\DataProvider\PhantomJsDataProvider
     */
    private $_phantomJsDataProvider;

    /**
     * Script directory path
     *
     * @var string
     */
    private $_scriptDirectoryPath;

    /**
     * Constructor
     */
    public function __construct()
    {
        $ds = DIRECTORY_SEPARATOR;

        $cookiesPath = realpath(
            dirname(__FILE__) . $ds . '..' . $ds . '..' . $ds . 'cookies'
        ) . $ds . 'phantom-cookies.txt';

        $this->_scriptDirectoryPath = realpath(
            dirname(__FILE__) . $ds . '..' . $ds . 'phantom_scripts'
        ) . $ds;

        $this->_phantomJsDataProvider = new PhantomJsDataProvider(null, $cookiesPath);
    }

    /**
     * Get email
     *
     * @param string $companyId Use $item['company_id']
     * @param string $contactName
     * @return string
     * @throws AtiApi\Exception
     */
    public function getEmail($companyId, $contactName)
    {
        // Send request
        $args = array(
            $companyId,
            $contactName
        );
        $this->_phantomJsDataProvider->setArguments($args)
            ->setScriptPath($this->_scriptDirectoryPath . 'get_email.js');

        $response = $this->_phantomJsDataProvider->getData();

        // Check for errors
        $this->_checkForErrorsAndAuthorizeOrRaiseException($response, 'get_email.js', $args);

        // No errors, try to get email
        $emailSignaturePos = strpos($response, 'EMAIL:');
        if ($emailSignaturePos == -1) {
            throw new Exception('Email signature not found');
        }

        $email = trim(
            substr($response, $emailSignaturePos + strlen('EMAIL:') + 1)
        );
        if (strlen($email) == 0) {
            throw new Exception('Empty email');
        }

        return $email;
    }

    /**
     * Set rate
     *
     * @param string $itemId Use $item['second_id']
     * @param string $companyId Use $item['company_id']
     * @param string $rateValue
     * @throws AtiApi\Exception
     * @return bool
     */
    public function setRate($itemId, $companyId, $rateValue)
    {
        // Check rate value
        if (!is_numeric($rateValue)) {
            throw new Exception('Rate value should be numeric');
        }

        // Send request
        $args = array(
            $itemId, $companyId, $rateValue
        );
        $response = $this->_phantomJsDataProvider->setArguments($args)
            ->setScriptPath($this->_scriptDirectoryPath . 'set_rate.js')
            ->getData();

        // Check for errors
        $this->_checkForErrorsAndAuthorizeOrRaiseException($response, 'set_rate.js', $args);

        return strpos($response, 'SUCCESS') > -1;
    }

    /**
     * Check and authorize
     *
     * @param string $response
     * @param string $scriptName
     * @param array $args
     * @throws AtiApi\Exception
     */
    protected function _checkForErrorsAndAuthorizeOrRaiseException($response, $scriptName, array $args = null)
    {
        if (strpos($response, 'ERROR') > -1) {

            // If not authorized, then authorize
            if (strpos($response, 'Not authorized') > -1) {

                if (!$this->_authorize()) {
                    throw new Exception('Can`t authorize');
                } else {

                    // Authorized, repeat request
                    $response = $this->_phantomJsDataProvider->setArguments($args)
                        ->setScriptPath($this->_scriptDirectoryPath . $scriptName)
                        ->getData();

                    // Error again...
                    if (strpos($response, 'ERROR') > -1) {
                        throw new Exception($response);
                    }

                }

            } else {
                // Authorized, but have error
                throw new Exception($response);
            }

        }
    }

    /**
     * Authorize
     *
     * @return bool
     */
    protected function _authorize()
    {
        $data = $this->_phantomJsDataProvider->setArguments(
            null
        )
        ->setScriptPath($this->_scriptDirectoryPath . 'auth.js')
        ->getData();

        return strpos($data, 'OK') > -1;
    }
} 